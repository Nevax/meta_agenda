<?php
/**
 * @package meta_agenda
 * @version 2.0
 */
/*
Plugin Name: Declic Agenda Widget
Plugin URI:
Description: Ajout des meta agenda au article
Author: Thomas FLEUROT, Antoine GRAVELOT
Version: 2.0
Author URI:
Text Domain: declic-agenda-widget
*/

function meta_agenda_add_custom_box() {
    add_meta_box(
        'meta_agenda_id',
        'Agenda',
        'meta_agenda_creation_box',
        'post'
    );
}

function meta_agenda_creation_box( $post ) {

	// Use nonce for verification
	wp_nonce_field( plugin_basename( __FILE__ ), 'myplugin_add_custom_box' );

	// The actual fields for data entry
	$meta_agenda_is_event = get_post_meta($post->ID, '_meta_agenda_is_event', true);
	$meta_agenda_date_debut = get_post_meta($post->ID, '_meta_agenda_date_debut', true);
	$meta_agenda_heure_debut = get_post_meta($post->ID, '_meta_agenda_heure_debut', true);
	$meta_agenda_minute_debut = get_post_meta($post->ID, '_meta_agenda_minute_debut', true);
	$meta_agenda_date_fin = get_post_meta($post->ID, '_meta_agenda_date_fin', true);
	$meta_agenda_heure_fin = get_post_meta($post->ID, '_meta_agenda_heure_fin', true);
	$meta_agenda_minute_fin = get_post_meta($post->ID, '_meta_agenda_minute_fin', true);
	$meta_agenda_rue = get_post_meta($post->ID, '_meta_agenda_rue', true);
	$meta_agenda_date_cp = get_post_meta($post->ID, '_meta_agenda_date_cp', true);
	$meta_agenda_date_ville = get_post_meta($post->ID, '_meta_agenda_date_ville', true);
	echo '<p><label for="meta_agenda_is_event"><strong>Cette article est un événement ? : </strong>';
		echo '<input type="radio" name="meta_agenda_is_event" value="1" '.((!empty($meta_agenda_is_event) && $meta_agenda_is_event == 1)  ? 'checked="checked"' : '').'> Oui
		<input type="radio" name="meta_agenda_is_event" value="0" '.(((!empty($meta_agenda_is_event) && $meta_agenda_is_event == 0) || empty($meta_agenda_is_event))  ? 'checked="checked"' : '').'> Non';
	echo '</label></p>';
	echo '<strong>Date</strong><hr/>';
	echo '<table>
		<tr>
			<td><label for="meta_agenda_date_debut">Date de début : </label></td>
			<td><input type="text" id="meta_agenda_date_debut" name="meta_agenda_date_debut" class="datepicker" value="'.(!empty($meta_agenda_date_debut) ? $meta_agenda_date_debut : date('d/m/Y')).'" readonly="readonly" /></td>
			<td><label for="meta_agenda_heure_debut">Heure de début : </label></td>
			<td><select id="meta_agenda_heure_debut" name="meta_agenda_heure_debut">';
			for($i=0; $i<24; $i++){
				if($i<10){
					$val = '0'.$i;
				}else{
					$val = $i;
				}
				if(!empty($meta_agenda_heure_debut) && $meta_agenda_heure_debut == $val){
					$selected = 'selected="selected"';
				}else{
					$selected = "";
				}
				echo '<option value="'.$val.'" '.$selected.'>'.$val.'</option>';
			}
			echo '</select>:';
			echo '<select id="meta_agenda_minute_debut" name="meta_agenda_minute_debut">';
			for($i=0; $i<60; $i++){
				if($i<10){
					$val = '0'.$i;
				}else{
					$val = $i;
				}
				if(!empty($meta_agenda_minute_debut) && $meta_agenda_minute_debut == $val){
					$selected = 'selected="selected"';
				}else{
					$selected = "";
				}
				echo '<option value="'.$val.'" '.$selected.'>'.$val.'</option>';
			}
			echo '</select>';
	echo'	</td>
		</tr>
		<tr>
			<td><label for="meta_agenda_date_fin">Date de fin : </label></td>
			<td><input type="text" id="meta_agenda_date_fin" name="meta_agenda_date_fin" class="datepicker" value="'.(!empty($meta_agenda_date_fin) ? $meta_agenda_date_fin : date('d/m/Y')).'" readonly="readonly" /></td>
			<td><label for="meta_agenda_heure_fin">Heure de fin : </label></td>
			<td><select id="meta_agenda_heure_fin" name="meta_agenda_heure_fin">';
				for($i=0; $i<24; $i++){
					if($i<10){
						$val = '0'.$i;
					}else{
						$val = $i;
					}
					if(!empty($meta_agenda_heure_fin) && $meta_agenda_heure_fin == $val){
						$selected = 'selected="selected"';
					}else{
						$selected = "";
					}
					echo '<option value="'.$val.'" '.$selected.'>'.$val.'</option>';
				}
				echo '</select>:';
				echo '<select id="meta_agenda_minute_fin" name="meta_agenda_minute_fin">';
				for($i=0; $i<60; $i++){
					if($i<10){
						$val = '0'.$i;
					}else{
						$val = $i;
					}
					if(!empty($meta_agenda_minute_fin) && $meta_agenda_minute_fin == $val){
						$selected = 'selected="selected"';
					}else{
						$selected = "";
					}
					echo '<option value="'.$val.'" '.$selected.'>'.$val.'</option>';
				}
				echo '</select>';
	echo'	</td>
		</tr>
	</table>
	<script>
	(function($) {
		$.datepicker.setDefaults($.datepicker.regional[ "" ]);
		$(".datepicker").datepicker($.datepicker.regional["fr"]);
	})(jQuery);
	</script>';
	echo '<strong>Adresse</strong><hr/>
	<table>
		<tr>
			<td><label for="meta_agenda_rue">Rue : </label></td>
			<td><input type="text" id="meta_agenda_rue" name="meta_agenda_rue" value="'.(!empty($meta_agenda_rue) ? $meta_agenda_rue : "").'" /></td>
		</tr>
		<tr>
			<td><label for="meta_agenda_date_cp">Code postal : </label></td>
			<td><input type="text" id="meta_agenda_date_cp" name="meta_agenda_date_cp" value="'.(!empty($meta_agenda_date_cp) ? $meta_agenda_date_cp :"").'" /></td>
		</tr>
		<tr>
			<td><label for="meta_agenda_date_ville">Ville : </label></td>
			<td><input type="text" id="meta_agenda_date_ville" name="meta_agenda_date_ville" value="'.(!empty($meta_agenda_date_ville) ? $meta_agenda_date_ville : "").'" /></td>
		</tr>
	</table>';
}

function meta_agenda_admin_init() {
	wp_deregister_script( 'jquery-ui-datepicker' );
	wp_register_script('jquery-ui-datepicker', plugins_url('js/jquery.ui.datepicker.js', __FILE__));
	wp_enqueue_script('jquery-ui-datepicker');
	wp_deregister_script( 'jquery-ui-datepicker-fr' );
	wp_register_script('jquery-ui-datepicker-fr', plugins_url('js/jquery.ui.datepicker-fr.js', __FILE__));
	wp_enqueue_script('jquery-ui-datepicker-fr');
	
	wp_register_style('jquery-ui-datepicker', plugins_url('css/jquery.ui.datepicker.css', __FILE__));
	wp_enqueue_style('jquery-ui-datepicker');

	wp_register_style('jquery.ui.theme', plugins_url('css/jquery.ui.theme.css', __FILE__));
	wp_enqueue_style('jquery.ui.theme');
	
	wp_register_style('style', plugins_url('css/style.css', __FILE__));
	wp_enqueue_style('style');
}

function meta_agenda_save_postdata( $post_id ) {
	if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE){
		return;
	}

  	if ( 'page' == $_POST['post_type'] ){
		if ( !current_user_can( 'edit_page', $post_id ) )
			return;
	}else{
		if ( !current_user_can( 'edit_post', $post_id ) )
			return;
	}

	update_post_meta($post_id, '_meta_agenda_is_event', $_POST['meta_agenda_is_event']);
	update_post_meta($post_id, '_meta_agenda_date_debut', $_POST['meta_agenda_date_debut']);
	update_post_meta($post_id, '_meta_agenda_heure_debut', $_POST['meta_agenda_heure_debut']);
	update_post_meta($post_id, '_meta_agenda_minute_debut', $_POST['meta_agenda_minute_debut']);
	update_post_meta($post_id, '_meta_agenda_date_fin', $_POST['meta_agenda_date_fin']);
	update_post_meta($post_id, '_meta_agenda_heure_fin', $_POST['meta_agenda_heure_fin']);
	update_post_meta($post_id, '_meta_agenda_minute_fin', $_POST['meta_agenda_minute_fin']);
	update_post_meta($post_id, '_meta_agenda_rue', $_POST['meta_agenda_rue']);
	update_post_meta($post_id, '_meta_agenda_date_cp', $_POST['meta_agenda_date_cp']);
	update_post_meta($post_id, '_meta_agenda_date_ville', $_POST['meta_agenda_date_ville']);
}

add_action( 'admin_init', 'meta_agenda_add_custom_box', 1 );
add_action('save_post', 'meta_agenda_save_postdata');
add_action('init', 'meta_agenda_admin_init');


class AgendaWidget extends WP_Widget {

	function AgendaWidget()
    {
        //les options du widget vont être le nom de la classe (classname) et sa description
        $widget_ops = array('classname' => 'AgendaWidget', 'description' => __( "Ajoute un widget agenda") );
        //les contrôles permettent de définir les dimensions du widget dans l'administration WP
        $control_ops = array('width' => 300, 'height' => 300);
        //dans la parenthèse centrale, écrivez le nom du widget qui apparaîtra dans l'admin de WP
        $this->WP_Widget('AgendaWidget', __('Ajoute un widget agenda'), $widget_ops, $control_ops);
    }

	function widget($args, $instance) {
		global $wpdb;
		$AgendaWidget_titre = empty($instance['AgendaWidget_titre']) ? null : $instance['AgendaWidget_titre']; //on récupère la catégorie à afficher

		extract($args);
		$agenda = get_posts(array('category' => 8));

		for($i=0; $i < count($agenda); $i++){
			$meta_agenda_date_debut = get_post_meta($agenda[$i]->ID, '_meta_agenda_date_debut', true);
			$meta_agenda_heure_debut = get_post_meta($agenda[$i]->ID, '_meta_agenda_heure_debut', true);
			$meta_agenda_minute_debut = get_post_meta($agenda[$i]->ID, '_meta_agenda_minute_debut', true);

			list($day, $month, $year) = explode('/', $meta_agenda_date_debut);
			$agenda[$i]->time_event_start = mktime ($meta_agenda_heure_debut, $meta_agenda_minute_debut, 0, $month, $day, $year);

			$meta_agenda_date_fin = get_post_meta($agenda[$i]->ID, '_meta_agenda_date_fin', true);
			$meta_agenda_heure_fin = get_post_meta($agenda[$i]->ID, '_meta_agenda_heure_fin', true);
			$meta_agenda_minute_fin = get_post_meta($agenda[$i]->ID, '_meta_agenda_minute_fin', true);

			list($day, $month, $year) = explode('/', $meta_agenda_date_fin);
			$agenda[$i]->time_event_stop = mktime ($meta_agenda_heure_fin, $meta_agenda_minute_fin, 0, $month, $day, $year);
		}
		$permut = true;
		while($permut){
			$permut=false;
			for($i=0; $i < count($agenda)-1; $i++){
				if($agenda[$i]->time_event_start > $agenda[$i+1]->time_event_start){
					$permut=false;
					$temp = $agenda[$i+1];
					$agenda[$i+1] = $agenda[$i];
					$agenda[$i] = $temp;
				}
			}
		}
		global $array_month;
		$event_divider = '';
		$i=0;
		$y=0;
		while($y<5 && isset($agenda[$i])){
			if(mktime() < $agenda[$i]->time_event_stop){
				$event_divider .= '<div class="event_divider">
					<span class="event_back">
						<span class="event_month">'.$array_month[date('n', $agenda[$i]->time_event_start)].'</span>
						<span class="event_day">'.date('d', $agenda[$i]->time_event_start).'</span>
					</span>
					<ul>
						<li class="calendar">
							<a href="'.get_permalink($agenda[$i]->ID).'">'.$agenda[$i]->post_title.'</a><br>
							<a href="'.get_permalink($agenda[$i]->ID).'" class="event-info">Voir l\'événement »</a>
						</li>
					</ul>
					<div style="clear:both"></div>
				</div>';
				$y++;
			}
			$i++;
		}
		echo '
		<div class="widget">
			<div class="homewtop"></div>
			<div class="homewmiddle">
				<h3>'.$AgendaWidget_titre.'</h3>
				'.$event_divider.'
			</div>
			<div class="homewbottom"></div>
		</div>';
	}

	function update($new_instance, $old_instance){
		$instance = $old_instance;
		$instance['AgendaWidget_titre'] = stripslashes($new_instance['AgendaWidget_titre']);
		return $instance;
	}
	function form($instance) {
		//on récupère tout d'abord les valeurs de chaque option.
		//les valeurs par défaut sont définies ici, par exemple 'posts'=>'5' défini le nombre de posts à afficher à 5 par défaut
		$instance = wp_parse_args((array) $instance, array('AgendaWidget_titre'=>''));

		//on stocke les valeurs, en s'assurant qu'ils vont s'afficher correctement
		$AgendaWidget_titre = $instance['AgendaWidget_titre'];

		// On affiche les champs de saisie, avec les valeurs des options récupérées précédemment
		echo '<label for="'.$this->get_field_name('AgendaWidget_titre').'">Titre : <input type="text" style="width: 200px;" id="'.$this->get_field_id('AgendaWidget_titre').'" name="'.$this->get_field_name('AgendaWidget_titre').'" value="'.$AgendaWidget_titre.'" /></label><br />';
	}
}

function AgendaWidgetInit(){
    register_widget('AgendaWidget');
}
add_action('widgets_init', 'AgendaWidgetInit');





?>
